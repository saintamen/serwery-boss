import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.*;


/**
 * Created by krzysztof on 12.11.17.
 */

@Startup
@Singleton
public class NumberGenerator {

    private static final Logger logger = Logger.getLogger(NumberGenerator.class);

    private List<Integer> winningNumbers;

    /**
     * W implementacji tej metody można posłużyć się klasą java.util.Random
     *
     * @return
     */
    private List<Integer> drawWinningNumbers() {
        Set<Integer> set = new HashSet<>();
        while (set.size() < 6) {
            set.add(new Random().nextInt(49) + 1);
        }

        return new ArrayList<>(set);
    }

    public List<Integer> getWinningNumbers() {
        return winningNumbers;
    }

    @PostConstruct
    public void generateNumbersOnStart() {
        winningNumbers = drawWinningNumbers();
        logger.info("Numbers : " + winningNumbers);
    }
}
